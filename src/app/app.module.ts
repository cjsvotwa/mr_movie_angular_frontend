import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BsDatepickerModule, } from 'ngx-bootstrap/datepicker';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { DatePipe } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatAutocomplete, MatAutocompleteModule } from '@angular/material/autocomplete';
import { AppComponent } from './app.component';
import { CategoryComponent } from './CategoryMasters/app.Category.Component';
import { AllCategoryComponent } from './CategoryMasters/app.AllCategory.Component';
import { EditCategoryComponent } from './CategoryMasters/app.EditCategory.Component';
import { MovieMasterComponent } from './MovieMaster/app.moviemaster.component';
import { AllMovieMasterComponent } from './MovieMaster/app.allmoviemaster.component';
import { AllMovieMasterRatedComponent } from './MovieMaster/app.allmoviemasterrated.component';
import { EditMovieComponent } from './MovieMaster/app.EditMovie.component';
import { MatSortModule, MatPaginatorModule, MatFormFieldModule, MatInputModule, MatSnackBar, MatSnackBarConfig, MatSnackBarModule } from '@angular/material';
import { LoginComponent } from './Login/app.LoginComponent';
import { AppAdminLayoutComponent } from './_layout/app-adminlayout.component';
import { AdminDashboardComponent } from './AdminDashboard/app.AdminDashboardComponent';
import { AdminLogoutComponent } from './Login/app.AdminLogout.Component';
import { AdminAuthGuardService } from './AuthGuard/AdminAuthGuardService';
import { UserAuthGuardService } from './AuthGuard/UserAuthGuardService';


@NgModule({
  declarations: [
    AppComponent,
    AppAdminLayoutComponent,
    CategoryComponent,
    AllCategoryComponent,
    EditCategoryComponent,
    MovieMasterComponent,
    AllMovieMasterComponent,
    AllMovieMasterRatedComponent,
    EditMovieComponent,
    LoginComponent,
    AdminLogoutComponent,
    AdminDashboardComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    MatTableModule,
    MatAutocompleteModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,

    RouterModule.forRoot([
      {
        path: 'Category',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'Add', component: CategoryComponent, canActivate: [AdminAuthGuardService] },
          { path: 'Edit/:categoryId', component: EditCategoryComponent, canActivate: [AdminAuthGuardService] },
          { path: 'All', component: AllCategoryComponent, canActivate: [AdminAuthGuardService] }
        ]
      },
      {
        path: 'Movie',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'Add', component: MovieMasterComponent, canActivate: [AdminAuthGuardService] },
          { path: 'Edit/:MovieID', component: EditMovieComponent, canActivate: [AdminAuthGuardService] },
          { path: 'All', component: AllMovieMasterComponent, canActivate: [AdminAuthGuardService] },
          { path: 'Rating', component: AllMovieMasterRatedComponent, canActivate: [AdminAuthGuardService] }
        ]
      },
      {
        path: 'Admin',
        component: AppAdminLayoutComponent,
        children: [
          { path: 'Dashboard', component: AdminDashboardComponent, canActivate: [AdminAuthGuardService] }

        ]
      },
      { path: 'Login', component: LoginComponent },
      { path: 'AdminLogout', component: AdminLogoutComponent },

      { path: '', redirectTo: "Login", pathMatch: 'full' },
      { path: '**', redirectTo: "Login", pathMatch: 'full' },


    ], { useHash: true })
  ],
  exports: [BsDatepickerModule],
  providers: [DatePipe, AdminAuthGuardService, UserAuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
