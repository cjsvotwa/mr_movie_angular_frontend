import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from './Services/app.Category.Service';
import { CategoryMasterViewModel } from './Models/app.CategoryViewModel';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  templateUrl: './app.AllCategoryComponent.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css', '../Content/vendor/font-awesome/css/font-awesome.min.css']

})

export class AllCategoryComponent implements OnInit {
  private _CategoryService;
  AllCategoryList: CategoryMasterViewModel[];
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['CategoryID', 'CategoryName', 'Status', 'EditAction', 'DeleteAction'];
  dataSource: any;
  
  


  constructor(private location: Location, private _Route: Router, private categoryService: CategoryService) {
    this._CategoryService = categoryService;

  
  }

  ngOnInit() {


    this._CategoryService.GetAllCategory().subscribe(
      AllCategory => {
        this.AllCategoryList = AllCategory
        this.dataSource = new MatTableDataSource(this.AllCategoryList);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
       
      },
      error => this.errorMessage = <any>error
    );

  }
  
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  Delete(categoryId): void {
    if (confirm("Are you sure to delete Category ?")) {
      this._CategoryService.DeleteCategory(categoryId).subscribe
        (
        response => {
          if (response.StatusCode == "200") {
            alert('Deleted Category Successfully');
            location.reload();
          }
          else {
            alert('Something Went Wrong');
            this._Route.navigate(['/Category/All']);
          }
        }
        )
    }
  }

}
