import { Component } from '@angular/core';
import { CategoryMasterModel } from './app.CategoryModel';
import { CategoryService } from './Services/app.Category.Service';
import { Router } from '@angular/router';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBarConfig, MatSnackBar } from '@angular/material';

@Component({
    templateUrl: './app.CategoryMaster.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css']
})

export class CategoryComponent {
    title = "Category Master";
    CategoryForms: CategoryMasterModel = new CategoryMasterModel();
    private _CategoryService;
    private responsedata: any;
    
    actionButtonLabel: string = 'Retry';
    action: boolean = false;
    setAutoHide: boolean = true;
    autoHide: number = 2000;
    verticalPosition: MatSnackBarVerticalPosition = 'top';
    horizontalPosition: MatSnackBarHorizontalPosition = 'center';



    constructor(private _Route: Router, public snackBar: MatSnackBar,  private categoryService: CategoryService) {
        this._CategoryService = categoryService;
    }
    output: any;
    onSubmit() {
      

        this._CategoryService.SaveCategory(this.CategoryForms).subscribe(
            response => 
            {
               
                this.output = response;
                if (this.output.StatusCode == "409") 
                {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Category Name Already Exists", this.action ? this.actionButtonLabel : undefined, config);
                   
                }
                else if (this.output.StatusCode == "200") 
                { 
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Saved Category Successfully", this.action ? this.actionButtonLabel : undefined, config);
                    this._Route.navigate(['/Category/All']);
                }
                else {
                    let config = new MatSnackBarConfig();
                    config.duration = this.setAutoHide ? this.autoHide : 0;
                    config.verticalPosition = this.verticalPosition;
                    this.snackBar.open("Something Went Wrong", this.action ? this.actionButtonLabel : undefined, config);
                   
                }
            }
        );



    }

}
