import { Injectable } from "@angular/core";
import { Observable, throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { CategoryMasterModel } from "../app.CategoryModel";
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { CategoryMasterViewModel } from "../Models/app.CategoryViewModel";
import { CategoryDropdownModel } from "../Models/app.CategoryDropdownModel";
// import { environment } from "src/app/Shared/environment";
import{environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})

export class CategoryService {
    private data: any;
    private apiUrl = environment.apiEndpoint + "/api/Category/";
    token: any;
    username: any;

    constructor(private http: HttpClient) {
        this.data = JSON.parse(localStorage.getItem('AdminUser'));
        this.token = this.data.token;
        this.username = this.data.username
    }

    // Save Category
  public SaveCategory(categoryMasterModel: CategoryMasterModel) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.post<any>(this.apiUrl, categoryMasterModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );

    }

    // Get All Category
  public GetAllCategory() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<CategoryMasterViewModel[]>(this.apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    // Get All Category List
  public GetAllActiveCategoryList() {
    var apiUrl = environment.apiEndpoint + "/api/CategoryDropdown/";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<CategoryDropdownModel[]>(apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    // Get All Category By ID
    public GetCategoryById(categoryId) {
        var editUrl = this.apiUrl + '/' + categoryId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<CategoryMasterViewModel>(editUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    // Update Category
    public UpdateCategory(categoryMasterModel: CategoryMasterModel) {
        var putUrl = this.apiUrl + '/' + categoryMasterModel.CategoryID;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.put<any>(putUrl, categoryMasterModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    public DeleteCategory(categoryId) {
        var deleteUrl = this.apiUrl + '/' + categoryId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.delete<any>(deleteUrl, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };



}
