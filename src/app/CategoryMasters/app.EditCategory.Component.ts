import { Component, OnInit } from '@angular/core';
import { CategoryService } from './Services/app.Category.Service';
import { CategoryMasterModel } from './app.CategoryModel';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBarConfig } from '@angular/material';
import { MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition, MatSnackBarConfig, MatSnackBar } from '@angular/material';

@Component({
  templateUrl: './app.EditCategoryComponent.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css']
})

export class EditCategoryComponent implements OnInit {
  title = "Edit Category Master";
  CategoryForms: CategoryMasterModel = new CategoryMasterModel();
  private _CategoryService;
  private responsedata: any;
  private CategoryID: string;
  errorMessage: any;

  actionButtonLabel: string = 'Retry';
  action: boolean = false;
  setAutoHide: boolean = true;
  autoHide: number = 2000;
  verticalPosition: MatSnackBarVerticalPosition = 'top';
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';

  constructor(private _Route: Router, private _routeParams: ActivatedRoute, public snackBar: MatSnackBar, private categoryService: CategoryService) {
    this._CategoryService = categoryService;
  }

  ngOnInit() {
    this.CategoryID = this._routeParams.snapshot.params['categoryId'];
    if (this.CategoryID != null) {
      var data = this._CategoryService.GetCategoryById(this.CategoryID).subscribe(
        Category => {
          this.CategoryForms.CategoryID = Category.CategoryID;
          this.CategoryForms.CategoryName = Category.CategoryName;
          this.CategoryForms.Status = Category.Status;
        },
        error => this.errorMessage = <any>error
      );
    }
  }


  onSubmit() {


    this._CategoryService.UpdateCategory(this.CategoryForms)
      .subscribe(response => {
        if (response.StatusCode == "200") {
          alert('Updated Category Successfully');
          this._Route.navigate(['/Category/All']);
        } else if (response.StatusCode == "409") {
          let config = new MatSnackBarConfig();
          config.duration = this.setAutoHide ? this.autoHide : 0;
          config.verticalPosition = this.verticalPosition;
          this.snackBar.open("Category Name Already Exists", this.action ? this.actionButtonLabel : undefined, config);
        }
      })
  }
}
