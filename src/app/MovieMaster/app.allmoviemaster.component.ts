import { Component, OnInit, ViewChild } from '@angular/core';
import { MovieService } from './Services/app.moviemaster.service';
import { MovieMasterViewModel } from './Models/app.MovieMasterViewModel';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  templateUrl: './app.allmoviemaster.component.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
    '../Content/vendor/metisMenu/metisMenu.min.css',
    '../Content/dist/css/sb-admin-2.css',
    '../Content/vendor/font-awesome/css/font-awesome.min.css'
  ]
})

export class AllMovieMasterComponent implements OnInit {
  private _movieService;
  MovieList: MovieMasterViewModel = new MovieMasterViewModel();
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['MovieID', 'MovieName', 'CategoryName', 'Rating', 'EditAction', 'DeleteAction'];
  dataSource: any;
  constructor(private _Route: Router, private movieService: MovieService) {
    this._movieService = movieService;
  }
  ngOnInit(): void {


    this._movieService.GetAllMovies().subscribe(
      allmovie => {
        this.MovieList = allmovie
        this.dataSource = new MatTableDataSource(allmovie);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error => this.errorMessage = <any>error
    );
  }


  Delete(MovieID) {
    if (confirm("Are you sure to delete Movie ?")) {
      this._movieService.DeleteMovie(MovieID).subscribe
        (
        response => {
          if (response.StatusCode == "200") {
            alert('Deleted Movie Successfully');
            location.reload();
          }
          else {
            alert('Something Went Wrong');
            this._Route.navigate(['/Movie/All']);
          }
        }
        )
    }
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
