import { Component, OnInit, ViewChild } from '@angular/core';
import { MovieService } from './Services/app.moviemaster.service';
import { ActiveMovieModel } from './Models/app.ActiveMovieModel';
import { Router } from '@angular/router';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';

@Component({
  templateUrl: './app.allmoviemasterrated.component.html',
  styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
    '../Content/vendor/metisMenu/metisMenu.min.css',
    '../Content/dist/css/sb-admin-2.css',
    '../Content/vendor/font-awesome/css/font-awesome.min.css'
  ]
})

export class AllMovieMasterRatedComponent implements OnInit {
  private _movieService;
  MovieList: ActiveMovieModel = new ActiveMovieModel();
  errorMessage: any;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['Rating', 'Count'];
  dataSource: any;
  constructor(private _Route: Router, private movieService: MovieService) {
    this._movieService = movieService;
  }
  ngOnInit(): void {


    this._movieService.GetAllActiveMoviesByRating().subscribe(
      allmovie => {
        this.MovieList = allmovie
        this.dataSource = new MatTableDataSource(allmovie);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
      },
      error => this.errorMessage = <any>error
    );
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}
