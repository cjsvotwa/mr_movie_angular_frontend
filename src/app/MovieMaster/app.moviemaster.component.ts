import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../CategoryMasters/Services/app.Category.Service';
import { MovieMasterModel } from './Models/app.MovieMasterModel';
import { CategoryDropdownModel } from '../CategoryMasters/Models/app.CategoryDropdownModel';
import { MovieService } from '../MovieMaster/Services/app.moviemaster.service';
import { Router } from '@angular/router';

@Component({
    templateUrl: './app.moviemaster.component.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
        '../Content/vendor/metisMenu/metisMenu.min.css',
        '../Content/dist/css/sb-admin-2.css',
        '../Content/vendor/font-awesome/css/font-awesome.min.css'
    ]
})

export class MovieMasterComponent implements OnInit {
    private _categoryService;
    private _movieService;

    AllActiveCategoryList: CategoryDropdownModel[];
    errorMessage: any;
    movieModel: MovieMasterModel = new MovieMasterModel();
    title = 'Add Movie';
    output: any;

    constructor(private _Route: Router,
        private categoryService: CategoryService,
        private movieService: MovieService
    ) {
        this._categoryService = categoryService;
        this._movieService = movieService;

    }

    ngOnInit(): void {
        this._categoryService.GetAllActiveCategoryList().subscribe(
            allActiveCategory => {
                this.AllActiveCategoryList = allActiveCategory
            },
            error => this.errorMessage = <any>error
        );
    }

    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }

    onSubmit() {
     

        this._movieService.SaveMovie(this.movieModel).subscribe(
            response => {
            this.output = response
            if (this.output.StatusCode == "409") {
                alert('Movie Already Exists');
            }
            else if (this.output.StatusCode == "200") {
                alert('Movie Saved Successfully');
                this._Route.navigate(['/Movie/All']);
            }
            else {
                alert('Something Went Wrong');
            }
        });
    }
}
