import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../CategoryMasters/Services/app.Category.Service';
import { CategoryDropdownModel } from '../CategoryMasters/Models/app.CategoryDropdownModel';
import { MovieService } from '../MovieMaster/Services/app.moviemaster.service';
import { MovieMasterModel } from './Models/app.MovieMasterModel';
import { Router, ActivatedRoute } from '@angular/router'

@Component({
    templateUrl: './app.EditMovie.component.html',
    styleUrls: ['../Content/vendor/bootstrap/css/bootstrap.min.css',
        '../Content/vendor/metisMenu/metisMenu.min.css',
        '../Content/dist/css/sb-admin-2.css',
        '../Content/vendor/font-awesome/css/font-awesome.min.css'
    ]
})


export class EditMovieComponent implements OnInit {
    private _categoryService;
    private _movieService;
    AllActiveCategoryList: CategoryDropdownModel[];
    errorMessage: any;
    movieModel: MovieMasterModel = new MovieMasterModel();
    MovieID: any;
    output: any;


    constructor(private _Route: Router, private _routeParams: ActivatedRoute, private categoryService: CategoryService, private movieService: MovieService) {
        this._categoryService = categoryService;
        this._movieService = movieService;
    }

    ngOnInit(): void {

        this.MovieID = this._routeParams.snapshot.params['MovieID'];
       
        // GetAllCategory

        this._categoryService.GetAllActiveCategoryList().subscribe(
            allActiveCategory => {
                this.AllActiveCategoryList = allActiveCategory
            },
            error => this.errorMessage = <any>error
        );

        // GetMovieByMovieID

        this._movieService.GetMovieByMovieID(this.MovieID).subscribe(
            movie => {
                this.movieModel = movie
    
            },
            error => this.errorMessage = <any>error
        );



    }


    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;

    }


    onSubmit() 
    {
        this._movieService.UpdateMovie(this.movieModel).subscribe(
            response => {
                this.output = response
                if (this.output.StatusCode == "409") {
                    alert('Movie Already Exists');
                }
                else if (this.output.StatusCode == "200") {
                    alert('Movie Saved Successfully');
                    this._Route.navigate(['/Movie/All']);
                }
                else {
                    alert('Something Went Wrong');
                }
            });
    }
}
