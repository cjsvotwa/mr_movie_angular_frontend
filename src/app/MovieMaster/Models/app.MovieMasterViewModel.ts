export class MovieMasterViewModel {

  public MovieID: number;
  public MovieName: string;
  public CategoryID: number;
  public CategoryName: string;
  public Rating: number;
}
