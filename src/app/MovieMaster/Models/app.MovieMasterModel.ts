export class MovieMasterModel {

  public MovieID: number;
  public MovieName: string;
  public CategoryID: number;
  public Rating: number;
}

