import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs'
import { catchError, tap } from 'rxjs/operators'
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { MovieMasterModel } from '../Models/app.MovieMasterModel';
import { MovieMasterViewModel } from '../Models/app.MovieMasterViewModel';
import { ActiveMovieModel } from '../Models/app.ActiveMovieModel';
// import { environment } from 'src/app/Shared/environment';
import{environment} from '../../../environments/environment';


@Injectable({
    providedIn: 'root'
})

export class MovieService
{
    private data: any;
    private apiUrl = environment.apiEndpoint + "/api/MovieMaster/";
    token: any;
    username: any;


    constructor(private http: HttpClient) {
        this.data = JSON.parse(localStorage.getItem('AdminUser'));
        this.token = this.data.token;
    }

    // Save Movie
    public SaveMovie(movieMasterModel: MovieMasterModel) {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post<any>(this.apiUrl, movieMasterModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );

    }

    public GetAmount(movieID: string, categoryId: string)
    {
        var apiUrl = environment.apiEndpoint + "/api/GetTotalAmount/";
        let AmountRequest = { "MovieId": movieID,"CategoryId":categoryId};

        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.post<string>(apiUrl, AmountRequest, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    // Get All Movies
    public GetAllMovies() {
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<MovieMasterViewModel[]>(this.apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

    // Get All Movies By Rating
    public GetAllActiveMoviesByRating() {
      var apiUrl = environment.apiEndpoint + "/api/GetRatingCategory";
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<ActiveMovieModel[]>(apiUrl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }

  // Get All Movies
  public GetAllActiveMovies(categoryId) {
    //var apiUrl = environment.apiEndpoint + "/api/AllActiveMovieMaster" + '/' + categoryId;;
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
    return this.http.get<ActiveMovieModel[]>(this.apiUrl, { headers: headers }).pipe(tap(data => data),
      catchError(this.handleError)
    );
  }


    // Get All Movies by MovieId
    public GetMovieByMovieID(movieId) {
        var editurl = this.apiUrl + movieId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.get<MovieMasterViewModel[]>(editurl, { headers: headers }).pipe(tap(data => data),
            catchError(this.handleError)
        );
    }


    // Update Movie
    public UpdateMovie(movieMasterModel: MovieMasterModel) {
        var updateurl = this.apiUrl + movieMasterModel.MovieID;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.put<any>(updateurl, movieMasterModel, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    public DeleteMovie(movieId) {
        var deleteUrl = this.apiUrl + '/' + movieId;
        let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
        headers = headers.append('Authorization', 'Bearer ' + `${this.token}`);
        return this.http.delete<any>(deleteUrl, { headers: headers })
            .pipe(
                catchError(this.handleError)
            );
    }

    private handleError(error: HttpErrorResponse) {
        if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
        } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
        }
        // return an observable with a user-facing error message
        return throwError('Something bad happened; please try again later.');
    };

}
